<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@home');
Route::get('/register', 'AuthController@register');
Route::get('/welcome', 'AuthController@welcome');

Route::get('/master', function(){
    return view('layout.master');
});

Route::get('/table', function(){
    return view('halaman.table');
});

Route::get('/data-tables', function(){
    return view('halaman.data-tables');
});

//CRUD Cast

//Create data Cast

// Masuk ke Form Cast
Route::get('/cast/create', 'CastController@create');

//Untuk Kirim inputan ke table cast
Route::post('/cast', 'CastController@store');

//Read Data Kategori

//Tampil semua data kategori
Route::get('/cast', 'CastController@index');

//Detail Kategori berdasarkan ID
Route::get('/cast/{cast_id}', 'CastController@show');

//Update Data Kategori

//Masuk ke Form Kategori berdasarkan ID
Route::get('/cast/{cast_id}/edit', 'CastController@edit');

//Untuk Update data inputan berdasarkan id
Route::put('/cast/{cast_id}', 'CastController@update');

//Delete data Cast

//Delete data berdasarkan ID
Route::delete('/cast/{cast_id}', 'CastController@destroy');
