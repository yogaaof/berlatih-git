@extends('layout.master')

@section('judul')
Halaman Daftar
@endsection

@section('content')
<form action="/welcome" method="get">
        @csrf
        <legend>Masukan Data</legend>
        <label>Fist name:</label><br>
        <input type="text" placeholder="Fist Name" name="firstname"><br><br>
        <label>Last name:</label><br>
        <input type="text" placeholder="Last Name" name="lastname"><br><br>
        <label>Gender:</label><br>
        <input type="radio" value="laki-laki" name="gender">Male<br>
        <input type="radio" value="perempuan" name="gender">Famale<br>
        <input type="radio" value="Other" name="gender">Other<br><br>
        <label>Nationality:</label><br>
        <select name="National">
            <option value="Id">Indonesia</option>
            <option value="My">Malaysia</option>
            <option value="Sg">Singapore</option>
        </select><br><br>
        <label>Language Spoken:</label><br>
        <input type="checkbox" value="Indonesia" name="bahasa">Bahasa Indonesia<br>
        <input type="checkbox" value="English" name="bahasa">English<br>
        <input type="checkbox" value="Other" name="bahasa">Other<br><br>
        <label>Bio:</label><br>
        <textarea name="message" cols="30" rows="10" placeholder="silakan masukan pesan anda..."></textarea><br>
        <input type="submit" value="Sign Up">
    </form>
@endsection