@extends('layout.master')

@section('judul')
    Halaman List Cast
@endsection

@section('content')

<a href="/cast/create" class="btn btn-primary btn-sm mb-4">Tambah Kategori</a>
<br>

    <table class="table table-bordered">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nama</th>
      <th scope="col">Umur</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($cast as $key => $item)
        <tr>
            <td>{{$key + 1}}</td>
            <td>{{$item->nama}}</td>
            <td>{{$item->umur}}</td>
            <form action="/cast/{{$item->id}}" method="POST">
              @csrf
              @method('delete')
            <td><a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
            <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
              <input type="submit" value="delete" class="btn btn-danger btn-sm">
            </form>
            </td>
        </tr>
    @empty
        <tr>
            <td>Data tidak di temukan di tabel Cast</td>
        </tr>
    @endforelse
  </tbody>
</table>
@endsection