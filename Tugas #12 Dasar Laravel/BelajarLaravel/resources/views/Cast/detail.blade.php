@extends('layout.master')

@section('judul')
    Halaman Detail Cast
@endsection

@section('content')

<h1 class="text-primary">{{$cast->nama}}</h1>
<p>Umur : {{$cast->umur}}</p>
<p>Bio : {{$cast->bio}}</p>

<a href="/cast" class="btn btn-secondary btn-sm">kembali</a>

@endsection
