<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form SignUp</title>
</head>
<body>
    <!-- awal heading -->
    <div>
        <h1>Buat Account Baru!</h1>
        <h2>Sign Up Form</h2>
    </div>
    <!-- akhir heading -->

    <!-- awal Form -->
    <form action="/welcome" method="get">
    <fieldset>
        @csrf
        <legend>Masukan Data</legend>
        <label>Fist name:</label><br>
        <input type="text" placeholder="Fist Name" name="firstname"><br><br>
        <label>Last name:</label><br>
        <input type="text" placeholder="Last Name" name="lastname"><br><br>
        <label>Gender:</label><br>
        <input type="radio" value="laki-laki" name="gender">Male<br>
        <input type="radio" value="perempuan" name="gender">Famale<br>
        <input type="radio" value="Other" name="gender">Other<br><br>
        <label>Nationality:</label><br>
        <select name="National">
            <option value="Id">Indonesia</option>
            <option value="My">Malaysia</option>
            <option value="Sg">Singapore</option>
        </select><br><br>
        <label>Language Spoken:</label><br>
        <input type="checkbox" value="Indonesia" name="bahasa">Bahasa Indonesia<br>
        <input type="checkbox" value="English" name="bahasa">English<br>
        <input type="checkbox" value="Other" name="bahasa">Other<br><br>
        <label>Bio:</label><br>
        <textarea name="message" cols="30" rows="10" placeholder="silakan masukan pesan anda..."></textarea><br>
        <input type="submit" value="Sign Up">
    </fieldset>
    </form>
    <!-- akhir Form -->
</body>
</html>